package com.murali;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.nio.charset.Charset;
/*
Explanation -
 Just the straightforward logic to sort the file chunk by chunk.
  1) Determine max number of integers we can store in memory.
  2) Load the correct amount of integers from the file that can fit at once into memory and call this, say partition 1.
  3) Sort partition 1 in memory and write to disk.
  4) Go back to step 2 and load next bunch of integers after partition 1 from original file and continue the same.
  5) We now have TOT_NUM_INT ÷ NUM_FIT_INTO_MEMORY number of files on disk that are each sorted within the partition.
  6) Just run a merge picking the top elements from each until we have requisite number of integers.
Complexity -
    time complexity is O(N log(N)) for sorting.
    space complexity is O (K) where K is max capacity of device.
Improvements -
1) In step 3 above, We can get away with writing only the largest N items in each partition to the disk. Since we will never use the remaining numbers even if all final numbers are coming from a single partition
2) I could use a bitwise approach if uniqueness and unsigned were guaranteed (eg - 0110 means number 1 and 2 is present in the list but 0 and 4 are not. 4 bits store info about 4 numbers.)
 */

/*
Assumptions -
1) Max memory usage allowed for this purpose on the system is known to be 4GB
2) The system has a HDD so disk I/O is comparatively more expensive than RAM access.
3) Each line in the file has value < 32 bits. Otherwise switch to Long or BigInteger as needed.
 */

public class Main {
    private static String inputFileName = "bigHugeFile.txt"; // ~200 GB file to read from
    private static long memoryCapacity = 4000000000L; // in bytes. assuming i can use 4GB for storing the numbers at a time after leaving memory for other stuff.
    private static long N = 10L;
    private static Integer sizeOfInt = 4; // in bytes, since an integer is 32 bits


    private ArrayList<String> createPartitionsOnDisk(String fileName, Long maxSimultaneousNumOfLines) throws IOException{
        BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(fileName))); // can specify custom input buffer size if needed.
        ArrayList<Integer> partialListOfItems = new ArrayList<>(); // already assumed that each line contains 32 bit int only.
        ArrayList<String> sortedFileNames = new ArrayList<>();
        String tempLine = in.readLine();
        int numberOfFiles = 0;
        do {
            for(Long currentLineNumber = 0L; currentLineNumber < maxSimultaneousNumOfLines && tempLine != null; currentLineNumber++) {
                partialListOfItems.add(Integer.parseInt(tempLine));
                tempLine = in.readLine();
            } // this loop fills up each partition.

            Collections.sort(partialListOfItems, Collections.reverseOrder()); // n log(n)
            numberOfFiles ++;
            String outputFileName = String.valueOf(numberOfFiles);
            Path out = FileSystems.getDefault().getPath(outputFileName);
            BufferedWriter writer = Files.newBufferedWriter(out);
            partialListOfItems.forEach((i) -> { // write the sorted partition into disk
                try {
                    writer.write(String.valueOf(i));
                    writer.newLine();
                } catch(Exception e) {
                    System.out.println("Caught exception when writing sorted int to partial file " + i + " with error " + e);
                }
            });
            writer.close();
            sortedFileNames.add(outputFileName);
            partialListOfItems.clear(); // clear the sorted partition in memory to make way for the next partition
        } while(tempLine != null); // stop when the original input file is empty

        in.close();
        return sortedFileNames;
    }

    /*
    Name: getTopNFromFile
    Desc: returns the largest N numbers from a given file where each line is an integer

    Inputs -
        String fileName = The file which needs to be processed.
        Long maxSimultaneousNumOfLines = The number of lines that can be held in memory at a time. This function assumes this number is valid.
        Long numberOfNumbersReqd = The largest numberOfNumbersReqd numbers that are required..
     Outputs -
        Prints the largest numberOfNumbersReqd from file at fileName without returning anything.

     Exceptions -
        Does not throw any exceptions.
     */

    private void getTopNFromFile(String fileName, Long maxSimultaneousNumOfLines, Long numberOfNumbers) {
        try {
            Path inputFilePath = FileSystems.getDefault().getPath(fileName);
            System.out.println("Going to work on " + inputFilePath + " and " + maxSimultaneousNumOfLines + " and " + N);
            if (Files.isRegularFile(inputFilePath, LinkOption.NOFOLLOW_LINKS)
                    && Files.isReadable(inputFilePath)) {
                // File is valid and we can read from it.

                ArrayList<String> partitionedFiles = createPartitionsOnDisk(fileName, maxSimultaneousNumOfLines);
                // now we have X files on disk all properly partitioned.
                // X is total input file size divided by memory capacity
                // do an X-way merge with X file pointers open concurrently.
                // I would take precautions if I felt even this X number of pointers won't fit into the memory concurrently.
                ArrayList<BufferedReader> partitionFileHandles = new ArrayList<>();
                ArrayList<Long> topElementsInEachPartition = new ArrayList<>();
                partitionedFiles.forEach((filename) -> {
                    try {
                        // make a reader for each partition
                        partitionFileHandles.add(new BufferedReader(new InputStreamReader(new FileInputStream(filename))));
                    } catch(FileNotFoundException e) {
                        System.out.println("Encountered file not found error when reading partition " + filename + " error is " + e);
                    }
                });

                for(int i = 0; i < partitionFileHandles.size(); i++) {
                    // assume each file will have at least one element and add it as the max element of each partition
                    topElementsInEachPartition.add(i, Long.valueOf(partitionFileHandles.get(i).readLine()));
                }

                HashSet<Integer> exhaustedPartitions = new HashSet<>();
                while(numberOfNumbers > 0) { // assuming that we will have enough numbers in our original file to get top N numbers.
                    // vary merge sort to extract top numberOfNumbers

                    Long currentMax = Long.MIN_VALUE;
                    int maxIndex = -1;
                    for(int i = 0; i < topElementsInEachPartition.size(); i++) {
                        if (!(exhaustedPartitions.contains(i))) {
                            if(topElementsInEachPartition.get(i) > currentMax) {
                                currentMax = topElementsInEachPartition.get(i);
                                maxIndex = i;
                            }
                        }
                    }
                    System.out.println(currentMax);
                    String newLine = partitionFileHandles.get(maxIndex).readLine();
                    if(newLine != null) {
                        topElementsInEachPartition.set(maxIndex, Long.valueOf(newLine));
                    } else {
                        exhaustedPartitions.add(maxIndex);
                    }
                    numberOfNumbers--;
                }
               partitionedFiles.forEach((filename) -> {
                    try {
                        Files.deleteIfExists(FileSystems.getDefault().getPath(filename));
                    } catch (Exception e) {
                        System.out.println("Caught exception when trying to delete file " + filename + " with error " + e);
                    }
                });

            } else {
                System.out.println("This file is invalid.. " + inputFilePath);
            }
        } catch (Exception e) {
            System.out.println("Encountered exception " + e);
        }

    }

    public static void main(String[] args) {
        Main s = new Main();


        // Dividing long and taking quotient only as we don't want decimal for number of lines to read
        s.getTopNFromFile(inputFileName, memoryCapacity/sizeOfInt, N);
    }
}
